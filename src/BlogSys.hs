module BlogSys
        ( Title  -- This module is meant to not export the constructors
        , Content
        , Post
        , User
        , Comment
        , getUser
        , getPosts
        , getPostTitle
        , getPostContent
        , getComments
        , getCommentTitle
        , getCommentContent
        ) where

type Title      = String
type Content    = String
newtype Post    = Post (Maybe Title, Content, [Comment])
newtype User    = User (Int, [Post])
newtype Comment = Comment (Maybe Title, Content)

getUser :: Int -> Maybe User
getUser id = let help ((u@(User (id', _))):r) | id' == id = Just u
                                              | null r    = Nothing
                                              | otherwise = help r
             in help db

getPosts :: User -> [Post]
getPosts (User (_,p)) = p

getPostTitle :: Post -> Maybe Title
getPostTitle (Post (t,_,_)) = t

getPostContent :: Post -> Content
getPostContent (Post (_,c,_)) = c

getComments :: Post -> [Comment]
getComments (Post (_,_,c)) = c

getCommentTitle :: Comment -> Maybe Title
getCommentTitle (Comment (t,_)) = t

getCommentContent :: Comment -> Content
getCommentContent (Comment (_,c)) = c


{-
The text's below are taken from the following sites

http://www.cupcakeipsum.com/
http://lorizzle.nl/
http://slipsum.com/
http://baconipsum.com/
-}

db = [ User (1, [ Post (Just "Sweets", "Lemon drops sweet roll cupcake biscuit cookie gummi bears powder gummies sweet. Oat cake marzipan sweet. Cupcake cotton candy halvah. Jujubes chocolate ice cream dragée. Chocolate bar candy bonbon. Jujubes bonbon apple pie. Croissant chocolate pastry tiramisu. Candy dragée soufflé icing. Tiramisu ice cream powder cake biscuit gummies.", [Comment (Nothing, "Gummi bears topping apple pie marshmallow I love toffee cake icing cake. Dessert pie toffee jelly topping bonbon lemon drops toffee. Caramels ice cream wafer chocolate cake fruitcake wafer."), Comment (Just "stuff", "Icing bear claw topping I love dessert. Icing marshmallow croissant sweet cookie jelly-o lollipop. Apple pie cookie marzipan brownie powder."), Comment (Nothing, "Powder ice cream sweet. Chocolate halvah icing I love oat cake icing muffin tiramisu dessert. Brownie pie cotton candy marshmallow muffin."), Comment (Nothing, "Icing oat cake I love muffin gummi bears halvah ice cream pie. Bear claw tootsie roll jelly beans I love cotton candy wafer. Lemon drops I love I love dragée I love jelly topping cookie. Biscuit biscuit lollipop icing dragée dessert."), Comment (Nothing, "Marshmallow tart pudding toffee toffee caramels. Croissant halvah pastry carrot cake cake dragée. Muffin dragée tootsie roll marzipan cheesecake I love.")])
                , Post (Just "GangSlang" , "Lorizzle my shizz dolizzle yo mamma amet, the bizzle its fo rizzle elit. Nullizzle sapizzle velit, sizzle you son of a bizzle, suscipit shizznit, shiz vel, arcu. Tellivizzle eget tortizzle. Sed erizzle. Check it out at dolor dapibizzle mammasay mammasa mamma oo sa crackalackin shizzle my nizzle crocodizzle. Maurizzle shiznit nibh dope fo shizzle. Boom shackalack in tortizzle. Pimpin' yo mamma rhoncus funky fresh. Hizzle yo mamma habitasse platea dictumst. Mah nizzle dope. Curabitizzle tellus crunk, pretizzle eu, mattizzle izzle, eleifend vitae, nunc. Own yo' suscipizzle. Owned shizznit sizzle that's the shizzle.", [Comment (Just "Wayyhoo", "In sagittis fo shizzle mah nizzle fo rizzle, mah home g-dizzle nizzle nisi. Yo mamma rhoncizzle, arcu non malesuada facilisis, break it down nulla aliquet mah nizzle, nizzle auctizzle izzle felis fo . Suspendisse volutpizzle boofron augue. Sizzle egestas lectizzle izzle libero. Prizzle shizznit blandizzle dang. Crazy shut the shizzle up, we gonna chung sizzle amet phat tincidunt, doggy sizzle ultricizzle sem, izzle vestibulizzle black nisi sit amizzle purizzle. Funky fresh that's the shizzle mammasay mammasa mamma oo sa its fo rizzle bizzle. Phasellus lobortizzle. Nulla lectizzle dang, convallizzle shizzle my nizzle crocodizzle, aliquizzle sit amet, hizzle rizzle, check out this. Vivamizzle pot. Vestibulizzle ante ipsum break yo neck, yall yo own yo' orci luctus izzle ultricizzle posuere pizzle Ghetto; In own yo' elizzle eu check it out brizzle condimentum. Bow wow wow my shizz pizzle, you son of a bizzle vizzle, for sure da bomb, commodo izzle, nizzle. Etizzle feugizzle, tortor eget tellivizzle shizzlin dizzle, lorizzle izzle ultricizzle lorem, id break it down mi urna vitae ass."), Comment (Just "For real?", "")])])
     , User (2, [ Post (Just "No, motherfucker", "Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.", [Comment (Just "Ehhm..", "Yeah, I like animals better than people sometimes... Especially dogs. Dogs are the best. Every time you come home, they act like they haven't seen you in a year. And the good thing about dogs... is they got different dogs for different people. Like pit bulls. The dog of dogs. Pit bull can be the right man's best friend... or the wrong man's worst enemy. You going to give me a dog for a pet, give me a pit bull. Give me... Raoul. Right, Omar? Give me Raoul."), Comment (Nothing, "The path of the righteous man is beset on all sides by the iniquities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of darkness, for he is truly his brother's keeper and the finder of lost children. And I will strike down upon thee with great vengeance and furious anger those who would attempt to poison and destroy My brothers. And you will know My name is the Lord when I lay My vengeance upon thee.")])
               , Post (Just "How 2 get FAT?", "Burgdoggen bresaola doner, biltong tenderloin picanha prosciutto pork chop jowl strip steak meatloaf pork. Meatball beef ribs burgdoggen, strip steak beef porchetta ball tip doner cupim shankle jerky flank pig tri-tip bacon. Tenderloin fatback meatball hamburger, tongue beef ribs shank turkey ribeye tail cow ground round bacon. T-bone pork beef, pork loin chicken corned beef short ribs meatloaf leberkas. Bresaola burgdoggen meatball picanha venison chuck meatloaf porchetta shank ground round tail. Pork chop swine shoulder corned beef short loin meatball leberkas brisket. Pastrami sausage meatloaf capicola shankle andouille leberkas ribeye ham bacon ball tip.", [Comment (Nothing, "Meatball drumstick flank boudin salami porchetta prosciutto jowl, pork chop picanha andouille ball tip shank frankfurter. Flank pig pork loin corned beef. Pork loin alcatra cupim shank, bacon meatloaf landjaeger andouille corned beef. Shoulder drumstick ham fatback kielbasa, burgdoggen beef swine flank kevin pig pancetta rump jerky chicken."), Comment (Nothing, "Short ribs picanha chuck corned beef filet mignon bacon pork belly venison jerky beef tail. Meatloaf jowl bresaola strip steak. T-bone hamburger filet mignon sausage flank porchetta picanha sirloin jowl spare ribs venison ribeye. Shankle short loin jowl landjaeger boudin turducken picanha pork loin burgdoggen fatback. Chuck kielbasa short loin, leberkas ball tip jowl capicola ham hock shoulder."), Comment (Nothing, "Shank fatback bacon picanha, ball tip pancetta pork loin pig ham hock venison bresaola boudin. Landjaeger tenderloin cupim spare ribs bresaola shank chuck pork belly pork t-bone jowl picanha corned beef ball tip. Short ribs ball tip short loin, ham sirloin shankle flank prosciutto t-bone cupim bresaola corned beef landjaeger burgdoggen chicken. Alcatra cow short loin ground round tongue.")])])]
