Aufgabe 2
=========

> module Aufgabe2 where

In der Vorlesung haben sie bereits einige Monaden kennengelernt. In dieser
Aufgabe sollen Sie das gelernte im praktischen Einsatz kennen lernen.

Bei dieser Aufgabe geht es um das Arbeiten mit einem 'Blog-System', in dem
`User`, `Post`s und `Comment`s gespeichert sind. All dies ist bereits
vorgegeben im Modul `BlogSys`. Für Sie relevant werden maximal folgende
Informationen zu diesem System sein:

type Title      = String
type Content    = String

getUser :: Int -> Maybe User

getPosts :: User -> [Post]
getPostTitle :: Post -> Maybe Title
getPostContent :: Post -> Content
getComments :: Post -> [Comment]
getCommentTitle :: Comment -> Maybe Title
getCommentContent :: Comment -> Content

> import BlogSys

Schreiben Sie nun die neue Funktion `headMay`, welche /eigentlich/ in der Prelude
sein sollte.

> headMay :: [a] -> Maybe a
>
> headMay = undefined

Schreiben Sie nun eine Funktion, die den ersten Buchstaben des Titels des
Kommentares des ersten Posts des Users mit der gegebenen ID zurückgibt,
falls dieser existiert

> getFirstLetterOfFirstCommentForFirstPostOfUserWithId = gflofcffpouwi
> gflofcffpouwi :: Int -> Maybe Char
> gflofcffpouwi = undefined

Wenn man sich nun nicht immer nur den ersten Eintrag zurückgeben lassen möchte,
empfiehlt es sich, eine Funktion `atMay` zu definieren.

> atMay :: [a] -> Int -> Maybe a
>
> atMay = undefined


Schreiben Sie nun die Funktionen `getNthPost` und `getNthComment`.

> getNthPost :: Int -> User -> Maybe Post
>
> getNthPost = undefined


> getNthComment :: Int -> Post -> Maybe Comment
>
> getNthComment = undefined

Schreiben Sie nun die Funktion:

> getNthLetterOfContentOfNthCommentOfNthPostOfUserWithId = what
>
> what :: Int -> Int -> Int -> Int -> Maybe Char
>
> what = undefined


> result = foldl foldLogic [] [ what 2 0 1 128, gflofcffpouwi 2, what 1 1 0 20
>                             , what 1 0 2 29, what 2 0 0 128, what 1 1 0 1063
>                             , what 2 0 0 188, what 2 1 2 194, what 1 0 4 94 ]

>
> foldLogic a (Just c) = a ++ [c]
> foldLogic a _        = a

