
{-# LANGUAGE TemplateHaskell #-}

module AdressSys where

import Control.Lens




type ID = Integer
type DB = [(ID,[Datum])]

newtype Age      = Age      Integer  deriving (Show,Eq)
newtype Name     = Name     String   deriving (Show,Eq)
newtype City     = City     String   deriving (Show,Eq)
newtype Email    = Email    String   deriving (Show,Eq)
newtype Phone    = Phone    String   deriving (Show,Eq)
newtype Street   = Street   String   deriving (Show,Eq)
newtype Gender   = Gender   String   deriving (Show,Eq)
newtype Postcode = Postcode String   deriving (Show,Eq)

data Adress = Adress Name Street Postcode City deriving (Show,Eq)
data Public = Public Name (Maybe Age) (Maybe Email) deriving (Show,Eq)
data Datum  = DName     Name
            | DStreet   Street
            | DPostcode Postcode
            | DEmail    Email
            | DPhone    Phone
            | DGender   Gender
            | DCity     City
            | DAge      Age
            deriving (Show,Eq)

$(makePrisms ''Datum)

isName :: Datum -> Maybe Name
isName = firstOf _DName

isStreet :: Datum -> Maybe Street
isStreet = firstOf _DStreet

isPostcode :: Datum -> Maybe Postcode
isPostcode = firstOf _DPostcode

isEmail :: Datum -> Maybe Email
isEmail = firstOf _DEmail

isPhone :: Datum -> Maybe Phone
isPhone = firstOf _DPhone

isGender :: Datum -> Maybe Gender
isGender = firstOf _DGender

isAge :: Datum -> Maybe Age
isAge = firstOf _DAge

isCity :: Datum -> Maybe City
isCity = firstOf _DCity



getName :: [Datum] -> Maybe Name
getName = firstOf (traverse._DName)

getStreet :: [Datum] -> Maybe Street
getStreet = firstOf (traverse._DStreet)

getPostcode :: [Datum] -> Maybe Postcode
getPostcode = firstOf (traverse._DPostcode)

getEmail :: [Datum] -> Maybe Email
getEmail = firstOf (traverse._DEmail)

getPhone :: [Datum] -> Maybe Phone
getPhone = firstOf (traverse._DPhone)

getGender :: [Datum] -> Maybe Gender
getGender = firstOf (traverse._DGender)

getAge :: [Datum] -> Maybe Age
getAge = firstOf (traverse._DAge)

getCity :: [Datum] -> Maybe City
getCity = firstOf (traverse._DCity)

getDataFromID :: ID -> DB -> Maybe [Datum]
getDataFromID iD = lookup iD



