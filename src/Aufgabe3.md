Aufgabe 3
=========
```haskell
module Aufgabe3 where

import Data.List
import Hui
```
Real world-Andwendung: List Applicative als numerisches Tool
------------------------------------------------------------

Sie sollen Maxima einer komplizierten Funktion finden. Die Funktion erlaubt keine analytische Berechnung 
der Maxima. Daher sollen Sie im Intervall [-10,10] für alle Parameter x y z w approximativ Maxima suchen. 
```haskell
komplizierteFunktion :: Double -> Double -> Double -> Double -> Double
```
Definieren Sie hierfür eine Funktion, die zusätzlich zum berechneten Wert die übergebenen Parameter zurückgibt. 
```haskell
berechnungMitEingabe :: Double -> Double -> Double -> Double -> (Double,(Double,Double,Double,Double))
berechnungMitEingabe x y z w =  undefined
```
Definieren mithilfe von `berechnungMitEingabe` eine Funktion `nBesteEingaben`, welche die n günstigsten 
Eingabeparameter-Tupel zusammen mit dem möglichst maximalen Ergebnis zurückgibt. 
```haskell
nBesteEingaben :: Int -> Double -> [(Double,(Double,Double,Double,Double))]
nBesteEingaben n d = take n $ sortOn (undefined) $ undefined
  where range = [(-10),(-(10-d))..10]
```

```haskell
result = show $ nBesteEingaben 10 1
```
