import Aufgabe3
import Hui

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)
import Test.HUnit

testData = [(72.47984345237802,(2.0,-5.0,7.0,1.0)),(72.4760070010783,(2.0,-8.0,7.0,1.0)),(72.46191340940976,(2.0,-2.0,7.0,1.0)),(72.42364516911707,(2.0,1.0,7.0,1.0)),(72.40259767762782,(2.0,8.0,7.0,1.0)),(72.36808715758247,(2.0,4.0,7.0,1.0)),(72.34631205975349,(2.0,-5.0,7.0,0.0)),(72.34247560845377,(2.0,-8.0,7.0,0.0)),(72.34112944881682,(2.0,5.0,7.0,1.0)),(72.32838201678523,(2.0,-2.0,7.0,0.0))]


{- TEST CASES-}

nBesteEingabeTest = testCase "Teste `nBesteEingabe`"
  $ assertEqual "Teste `nBesteEingabe 10 1` == hard coded test data " (testData) 
  $ nBesteEingaben 10 1

tests = [nBesteEingabeTest]

main :: IO ()
main = defaultMain tests