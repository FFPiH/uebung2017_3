import Aufgabe2
import BlogSys

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)
import Test.HUnit


{- TEST DATA -}

listWithElements = [1..10]
emptyList = []

{- TEST CASES-}

headMayTest1 = testCase "headMay leere Liste"
  $ assertEqual "testing headMay with empty list" Nothing (headMay emptyList :: Maybe Integer)

headMayTest2 = testCase "headMay nicht leere Liste"
  $ assertEqual "testing headMay with non-empty List" (Just 1) (headMay listWithElements :: Maybe Integer)

gflofcffpouwiTest1 = testCase "gflofcffpouwi Test1"
  $ assertEqual "gflofcffpouwi Test for User 1" Nothing $ gflofcffpouwi 1

gflofcffpouwiTest2 = testCase "gflofcffpouwi Test2"
  $ assertEqual "gflofcffpouwi Test for User 2" (Just 'E') $ gflofcffpouwi 2 


atMayTest1 = testCase "atMay Test 1"
  $ assertEqual "atMay Test with element in list" (Just 4) $ atMay listWithElements 3


atMayTest2 = testCase "atMay Test 2"
  $ assertEqual "atMay Test with element in list" (Just 7) $ atMay listWithElements 6


atMayTest3 = testCase "atMay Test 3"
  $ assertEqual "atMay Test with last element of list" (Just 10) $ atMay listWithElements 9


atMayTest4 = testCase "atMay Test 4"
  $ assertEqual "atMay Test with index out of list" Nothing $ atMay listWithElements 10

atMayTest5 = testCase "atMay Test 5"
  $ assertEqual "atMay Test with empty list" Nothing (atMay emptyList 1 :: Maybe Integer)

getNthPostTest1 = testCase "getNthPost Test 1"
  $ assertEqual "getNthPost Test for User 1 Post 1" (Just $ Just "Lemon drops sweet roll cupcake biscuit cookie gummi bears powder gummies sweet. Oat cake marzipan sweet. Cupcake cotton candy halvah. Jujubes chocolate ice cream dragée. Chocolate bar candy bonbon. Jujubes bonbon apple pie. Croissant chocolate pastry tiramisu. Candy dragée soufflé icing. Tiramisu ice cream powder cake biscuit gummies.") $ (fmap.fmap) getPostContent $ getNthPost 0 <$> getUser 1

getNthPostTest2 = testCase "getNthPost Test 2"
  $ assertEqual "getNthPost Test for User 1 Post 2" (Just $ Just "Lorizzle my shizz dolizzle yo mamma amet, the bizzle its fo rizzle elit. Nullizzle sapizzle velit, sizzle you son of a bizzle, suscipit shizznit, shiz vel, arcu. Tellivizzle eget tortizzle. Sed erizzle. Check it out at dolor dapibizzle mammasay mammasa mamma oo sa crackalackin shizzle my nizzle crocodizzle. Maurizzle shiznit nibh dope fo shizzle. Boom shackalack in tortizzle. Pimpin' yo mamma rhoncus funky fresh. Hizzle yo mamma habitasse platea dictumst. Mah nizzle dope. Curabitizzle tellus crunk, pretizzle eu, mattizzle izzle, eleifend vitae, nunc. Own yo' suscipizzle. Owned shizznit sizzle that's the shizzle.") $ (fmap.fmap) getPostContent $ getNthPost 1 <$> getUser 1

getNthPostTest3 = testCase "getNthPost Test 3"
  $ assertEqual "getNthPost Test for User 2 Post 1" (Just $ Just "Your bones don't break, mine do. That's clear. Your cells react to bacteria and viruses differently than mine. You don't get sick, I do. That's also clear. But for some reason, you and I react the exact same way to water. We swallow it too fast, we choke. We get some in our lungs, we drown. However unreal it may seem, we are connected, you and I. We're on the same curve, just on opposite ends.") $ (fmap.fmap) getPostContent $ getNthPost 0 <$> getUser 2

getNthPostTest4 = testCase "getNthPost Test 4"
  $ assertEqual "getNthPost Test for User 2 Post 2" (Just $ Just "Burgdoggen bresaola doner, biltong tenderloin picanha prosciutto pork chop jowl strip steak meatloaf pork. Meatball beef ribs burgdoggen, strip steak beef porchetta ball tip doner cupim shankle jerky flank pig tri-tip bacon. Tenderloin fatback meatball hamburger, tongue beef ribs shank turkey ribeye tail cow ground round bacon. T-bone pork beef, pork loin chicken corned beef short ribs meatloaf leberkas. Bresaola burgdoggen meatball picanha venison chuck meatloaf porchetta shank ground round tail. Pork chop swine shoulder corned beef short loin meatball leberkas brisket. Pastrami sausage meatloaf capicola shankle andouille leberkas ribeye ham bacon ball tip.") $ (fmap.fmap) getPostContent $ getNthPost 1 <$> getUser 2

testResult = testCase "Test getNthLetterOfContentOfNthCommentOfNthPostOfUserWithId"
  $ assertEqual "Testing result" "wEll doné" result

tests = [ headMayTest1, headMayTest2, gflofcffpouwiTest1, gflofcffpouwiTest2
        , atMayTest1, atMayTest2, atMayTest3, atMayTest4, atMayTest5
        , getNthPostTest1, getNthPostTest2, getNthPostTest3, getNthPostTest4
        , testResult]

main :: IO ()
main = defaultMain tests
