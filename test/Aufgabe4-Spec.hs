import Aufgabe4

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)

import Test.HUnit

testData = [(Rot,Land Frankreich [Schweiz,Deutschland,Luxemburg]),(Gruen,Land Deutschland [Frankreich,Schweiz,Oesterreich,Luxemburg,Polen,Niederlande,Belgien,Tschechien]),(Rot,Land Niederlande [Deutschland,Belgien]),(Gelb,Land Belgien [Frankreich,Deutschland,Luxemburg]),(Rot,Land Polen [Tschechien,Deutschland]),(Rot,Land Oesterreich [Schweiz,Deutschland,Tschechien]),(Gelb,Land Schweiz [Frankreich,Oesterreich,Deutschland]),(Rot,Land Island []),(Blau,Land Luxemburg [Frankreich,Deutschland]),(Gelb,Land Tschechien [Oesterreich,Polen,Deutschland])]


einfaerbenMTest = testCase "Teste `einfaerbenM`"
  $ assertEqual "Teste `head einfaerbenM defaultMap` == hard coded test data" (testData) 
  $ (head $ einfaerbenM defaultMap)

einfaerbenLCTest = testCase "Teste `einfaerbenLC`"
  $ assertEqual "Teste `head einfaerbenLC defaultMap` == hard coded test data" (testData) 
  $ (head $ einfaerbenLC defaultMap)

tests = [einfaerbenMTest,einfaerbenLCTest]

main :: IO ()
main = defaultMain tests
