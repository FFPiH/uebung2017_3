import Aufgabe1
import AdressSys

import Test.Framework.Providers.HUnit (testCase)
import Test.Framework.Runners.Console (defaultMain)
import Test.HUnit


{- TEST DATA -}

resultManyAdresses = [Just (Adress (Name "Mona D.") (Street "Intuition 6") (Postcode "42317") (City "Portland,Oregano")),Nothing,Nothing,Just (Adress (Name "Hans Joachim Meyer") (Street "Viktoriastra\223e 22") (Postcode "33602") (City "Bielefeld")),Nothing,Nothing,Nothing,Nothing,Nothing,Just (Adress (Name "Alonzo Storch") (Street "Antenne 2") (Postcode "12346") (City "Dingenskirchen"))] 

{- TEST CASES-}

getAdressATest = testCase "Teste getAdressA"
  $ assertEqual "`getAdressM db` und `getAdressA db` sollten die gleichen Adressdaten raussuchen" (getDataFromID 1 db1 >>= getAdressM) 
  $ getDataFromID 1 db1 >>= getAdressA

getPublicATest = testCase "Teste getPublicA"
  $ assertEqual "`getPublicM db` und `getPublicA db` sollten die gleichen Adressdaten raussuchen" (getDataFromID 1 db1 >>= getPublicM) 
  $ getDataFromID 1 db1 >>= getPublicA

getManyAdressTest = testCase "Teste getAdressA"
  $ assertEqual "Das Ergebnis für `getManyAdresses [1..5] [db1,db2]` ist falsch" resultManyAdresses
  $ getManyAdresses [1..5] [db1,db2]


tests = [getManyAdressTest,getPublicATest,getAdressATest]

main :: IO ()
main = defaultMain tests